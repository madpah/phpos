/*
	App configuration
*/
define([
	"libs/core",
    "models/PHSMSAPI",
	"helpers/locale",
], function(core, PHSMSAPI, locale) {

	//webix.codebase = "libs/webix/";
	//CKEditor requires full path
	webix.codebase = document.location.href.split("#")[0].replace("index.html", "") + "libs/webix/";

    webix.i18n.locales["en-GB"] = {
        groupDelimiter:",",
        groupSize:3,
        decimalDelimiter:".",
        decimalSize:2,

        dateFormat:"%d/%m/%Y",
        timeFormat:"%h:%i %A",
        longDateFormat:"%d %F %Y",
        fullDateFormat:"%d/%m/%Y %h:%i %A",

        price:"£{obj}",
        priceSettings: {
            groupDelimiter: ",",
            groupSize: 3,
            decimalDelimiter: ".",
            decimalSize: 2
        },
        fileSize: ["b","Kb","Mb","Gb","Tb","Pb","Eb"],

        calendar: {
            monthFull:["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
            monthShort:["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            dayFull:["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            dayShort:["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            hours: "Hours",
            minutes: "Minutes",
            done:"Done",
            clear: "Clear",
            today: "Today"
        },

        controls: {
            select:"Select"
        },
        dataExport: {
            page:"Page",
            of:"of"
        }
    };

	webix.i18n.setLocale("en-GB");

    webix.attachEvent("onBeforeAjax", function(mode, url, data, request, headers) {
        var authorizationHeader = "PHSMS POST=";
        var post = webix.storage.local.get('posTerminal');
        if (post && post.uuid) {
            authorizationHeader += post.uuid;
        }
        var sessionHeaders = webix.storage.session.get('ajax_headers');
        if (sessionHeaders && sessionHeaders.Authorization) {
            authorizationHeader += ":" + sessionHeaders.Authorization;
        }
        //console.debug("Using auth header: " + authorizationHeader);
        headers["Authorization"] = authorizationHeader;
    });

    if(!webix.env.touch && webix.ui.scrollSize && webix.CustomScroll)
        webix.CustomScroll.init();

	//configuration
	var app = core.create({
		id:			"phsms-pos",
		name:		"PHSMS POS",
		version:	"0.1",
		debug:		true,
		start:		"/login"
	});

	app.use(locale);

	return app;
});