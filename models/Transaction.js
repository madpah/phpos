define([
    "app",
    "models/PHSMSAPI",
    "models/POSTerminal",
    "models/TaxClass",
    "models/PaymentType",
    "models/User"
], function(app, PHSMSAPI, POSTerminal, TaxClass, PaymentType, User) {

    var transactionData = null;
    var transactionLineIdIndex, paymentIdIndex;

    var transactionLineData = new webix.DataCollection({
        id: "transactionLineDataCollection",
        data: {},
    });

    var paymentsData = new webix.DataCollection({
        id: "transactionPaymentsDataCollection"
    });

    function _initNewTransaction() {
        transactionData = {
            number: POSTerminal.getNextTransactionNumber(),
            started_at: new Date().toLocaleString()
        };
        transactionLineData.clearAll();
        paymentsData.clearAll();
        transactionLineIdIndex = 1;
        paymentIdIndex = 1;

        app.callEvent("transactionStarted", [ transactionData ]);
    }

    function _nextTransaction() {
        _initNewTransaction();
    }

    function _addTransactionLine(lineObj) {
        if (!lineObj.id) {
            lineObj.id = transactionLineIdIndex;
        }

        // Compute some fields
        lineObj.name = lineObj.inventory_name;
        lineObj.inventory_unit_price = parseFloat(lineObj.inventory_unit_price);
        lineObj.line_total_tax = Math.round((lineObj.inventory_unit_tax * lineObj.quantity) * 100) / 100;
        lineObj.line_total = Math.round((lineObj.inventory_unit_price * lineObj.quantity) * 100) / 100;
        lineObj.tax_class_code = TaxClass.getCodeForClass(lineObj.tax_class_id);
        lineObj.tax_class_rate = parseFloat(TaxClass.getRateForClass(lineObj.tax_class_id));
        lineObj.tax_rate_id = parseInt(TaxClass.getRateIdForClass(lineObj.tax_class_id));

        transactionLineData.add(lineObj, transactionLineIdIndex);
        transactionLineIdIndex++;
        app.callEvent("transactionLineAdded");
    }

    function _addMisSaleTransactionLine(price, description, tax_class_id) {
        var miscInventory = POSTerminal.getConfig().system_inventory.MISC;

        var newLineObj = {
            inventory_id: miscInventory.id,
            inventory_name: description,
            inventory_barcode: "",
            inventory_unit_price: price,
            //inventory_unit_tax: i.unitTax,
            tax_class_id: tax_class_id,
            quantity: 1
        };

        /*
         @todo: Here we are calculating the unit tax, whereas we noramlly rely on server-side for this
         */
        var rate = parseFloat(TaxClass.getRateForClass(tax_class_id)) / 100;
        var newUnitTax = Math.round((newLineObj.inventory_unit_price * rate) * 100) / 100;
        newLineObj.inventory_unit_tax = newUnitTax;

        _addTransactionLine(newLineObj);
    }

    function _refundTransactionLine(lineId) {
        var lineObj = transactionLineData.getItem(lineId);
        lineObj.quantity = 0 - lineObj.quantity;
        lineObj.inventory_unit_price = 0 - lineObj.inventory_unit_price;
        lineObj.inventory_unit_tax = 0 - lineObj.inventory_unit_tax;
        lineObj.line_total_tax = 0 - lineObj.line_total_tax;
        lineObj.line_total = 0 -  lineObj.line_total;

        app.callEvent("transactionLineUpdated");
    }

    function _removeTransactionLine(lineId) {
        transactionLineData.remove(lineId);
        app.callEvent("transactionLineRemoved");
    }

    function _changeTransactionLineQuantity(lineId, newQuantity) {
        var lineObj = transactionLineData.getItem(lineId);
        lineObj.quantity = parseInt(newQuantity);
        lineObj.line_total_tax = Math.round((lineObj.inventory_unit_tax * lineObj.quantity) * 100) / 100;
        lineObj.line_total = Math.round((lineObj.inventory_unit_price * lineObj.quantity) * 100) / 100;
        app.callEvent("transactionLineUpdated");
    }

    function _changeTransactionLinePrice(lineId, newUnitPrice) {
        var lineObj = transactionLineData.getItem(lineId);
        lineObj.inventory_unit_price = parseFloat(newUnitPrice);
        /*
        @todo: Here we are calculating the unit tax, whereas we noramlly rely on server-side for this
         */
        var rate = lineObj.tax_class_rate / 100;
        var newUnitTax = Math.round((lineObj.inventory_unit_price * rate) * 100) / 100;
        lineObj.inventory_unit_tax = newUnitTax;

        lineObj.line_total_tax = Math.round((lineObj.inventory_unit_tax * lineObj.quantity) * 100) / 100;
        lineObj.line_total = Math.round((lineObj.inventory_unit_price * lineObj.quantity) * 100) / 100;
        app.callEvent("transactionLineUpdated");
    }

    function _addPayment(paymentType, amount) {
        var paymentTypeId = PaymentType.getIdForName(paymentType);

        if (null == paymentTypeId) {
            webix.messsage({
                type: "error",
                text: "Unknown payment type: '" + paymentType + "'"
            })
            return false;
        }

        console.debug("Taking payment " + amount + " in " + paymentType);

        paymentsData.add({
            id: paymentIdIndex,
            payment_type_id: paymentTypeId,
            payment_type_name: paymentType,
            amount: parseFloat(amount),
            dateTime: new Date().toLocaleString()
        }, paymentIdIndex);
        paymentIdIndex++;
        app.callEvent("transactionPaymentAdded");
    }

    function _getTransactionTotal() {
        var total = 0;
        transactionLineData.data.each(function (obj) {
            total = Math.round((total + obj.line_total) * 100) / 100;
        });
        return total;
    }

    function _getTotalPayments() {
        var total = 0;
        paymentsData.data.each(function (obj) {
            total = Math.round((total + obj.amount) * 100) / 100;
        });
        return total;
    }

    function _getTotalOutstanding() {
        return Math.round((_getTransactionTotal() - _getTotalPayments()) * 100) / 100;
    }

    function _isInProgress() {
        if (transactionLineData.count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function _printTransaction(callback, isComplete = false) {
        var date = new Date();
        var data = {
            location: {
                address: 'Empire Works, Railway Street, Ramsbottom, Bury, BL0 9AS',
                telephone: '01706 826 479',
                website: 'www.clarkcraft.co.uk'
            },
            transaction: {
                isCompleted: isComplete,
                posUserName: User.getCurrentUser().full_name,
                transactionDate: date.toLocaleDateString(),
                transactionTime: date.toLocaleTimeString(),
                transactionNumber: transactionData.number,
                transactionLines: [],
                transactionTotal: '£' + _getTransactionTotal(),
                transactionPayments: [],
                taxBreakdown: []
            }
        };
        // Add Transaciton Lines
        transactionLineData.data.each(function (obj) {
            var line = {
                id: obj.id,
                description: obj.inventory_name,
                barcode: obj.inventory_barcode,
                quantity: obj.quantity,
                unitPrice: '£' + obj.inventory_unit_price,
                vatCode: obj.tax_class_code
            };
            data.transaction.transactionLines.push(line);
        });
        // Add Payments Made
        paymentsData.data.each(function (obj) {
            var payment = {
                id: obj.id,
                type: obj.payment_type_name,
                amount: '£' + Math.round(obj.amount * 100) / 100
            };
            data.transaction.transactionPayments.push(payment);
        });
        // Add Tax Breakdowns
        var taxBreakdowns = [];
        transactionLineData.data.each(function (obj) {
            if (!taxBreakdowns[obj.tax_class_id]) {
                taxBreakdowns[obj.tax_class_id] = {
                    id: obj.tax_class_id,
                    code: obj.tax_class_code,
                    rate: obj.tax_class_rate,
                    goodsValue: 0.00,
                    taxAmount: 0.00
                };
            }
            taxBreakdowns[obj.tax_class_id].goodsValue += obj.line_total;
            taxBreakdowns[obj.tax_class_id].taxAmount += obj.line_total_tax;
        });
        for (var i = 0, n = taxBreakdowns.length; i < n; i++) {
            if (taxBreakdowns[i] && taxBreakdowns[i].code) {
                data.transaction.taxBreakdown.push(taxBreakdowns[i]);
            }
        };

        console.debug(POSTerminal.getConfig());
        var url = "/print.html?template=" + POSTerminal.getConfig().receiptTemplate + "&data=" + encodeURIComponent(JSON.stringify(data));
        var printWindow = window.open(url, "printWindow", "fullscreen=0,height=600,width=350,location=0,menubar=0,resizable=0,scrollbars=0,status=0,titlebar=0,toolbar=0");
        //printWindow.print();
        //printWindow.close();

        if (typeof callback === "function") {
            callback();
        }
    }

    // Reset if cancelling transaction
    app.on("cancelCurrentSaleNow", function () {
        _initNewTransaction();
    });

    // Check if we are fully paid now
    app.on("transactionPaymentAdded", function () {
        console.debug("Payment Added");
        if (_getTotalPayments() >= _getTransactionTotal()) {
            console.debug("Fully Paid");
            app.callEvent("transactionFullyPaid");
        }
    });

    // Finalise transaction - commit to server
    app.on("transactionBeginFinalisation", function () {
        console.debug("Beginning finalisation");

        var transaction = new Object;
        transaction.info = transactionData;
        transaction.lines = new Array;
        transactionLineData.data.each(function (obj) {
           transaction.lines.push(obj);
        });
        transaction.payments = new Array;
        paymentsData.data.each(function (obj) {
            transaction.payments.push(obj);
        })

        webix.ajax().sync().headers({"Content-type": "application/json"}).put(PHSMSAPI.getApiUrl('/transaction/store'), JSON.stringify(transaction), {
            success: function (text, data, XmlHttpRequest) {
                var response = data.json();
                if (response.status == "SUCCESS") {
                    app.callEvent("transactionFinalisation_StoreComplete");
                } else {
                    webix.message({
                        type: "error",
                        text: "Saving transaction failed: " + XmlHttpRequest.statusCode + ": " + XmlHttpRequest.statusText + ": " + response.status
                    })
                }
            },
            error: function (text, data, XmlHttpRequest) {
                webix.message({
                    type: "error",
                    text: "Error saving transaction: " + XmlHttpRequest.statusCode + ": " + XmlHttpRequest.statusText
                })
            }
        });
    });

    // Finalisation - store complete, next to print
    app.on("transactionFinalisation_StoreComplete", function () {
        console.debug("Printing Transaction");
        _printTransaction(function () {
            app.callEvent("transactionCompleted");
        }, true);
    });

    app.on("printCurrentTransaction", function () {
       if (_isInProgress()) {
           _printTransaction();
       }
    });

    return {
        start: _initNewTransaction,
        nextTransaction: _nextTransaction,
        tData: transactionData,
        lData: transactionLineData,
        pData: paymentsData,
        addLine: _addTransactionLine,
        addMiscSaleLine: _addMisSaleTransactionLine,
        refundLine: _refundTransactionLine,
        removeLine: _removeTransactionLine,
        changeLineQuantity: _changeTransactionLineQuantity,
        changeLinePrice: _changeTransactionLinePrice,
        addPayment: _addPayment,
        getTotal: _getTransactionTotal,
        getTotalPaid: _getTotalPayments,
        getTotalOutstanding: _getTotalOutstanding,
        isInProgress: _isInProgress,
    }
});