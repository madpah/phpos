define([
    "app",
    "models/PHSMSAPI",
], function(app, PHSMSAPI) {

    var zReadValues = new webix.DataCollection({
        id: "zReadValueData",
        data: {},
        url: PHSMSAPI.getApiUrl('/pos-terminal/x-read'),
    });

    function _submit() {
        var zReadPayload = new Object;
        zReadValues.data.each(function (row) {
            zReadPayload[row.id] = {
                payment_type_id: row.id,
                payment_type_name: row.Name,
                counted_total: parseFloat(row.Total),
                count_data: JSON.stringify(row.data)
            };
        });
        console.debug(zReadPayload);

        webix.ajax().sync().headers({
            'Content-type': 'application/json'
        }).post(PHSMSAPI.getApiUrl('/z-read/submit'), zReadPayload, {
            "success": function (text, data, XmlHttpRequest) {

                app.callEvent("completedSubmittingZRead", [ data.json() ]);
            },
            "error": function (text, data, XmlHttpRequest) {
                webix.message({
                    type: "error",
                    text: "Failure submitting Z Read: " + XmlHttpRequest.statusCode + ": " + XmlHttpRequest.statusText
                });
            }
        });
    }

    function _reset(reset_data) {
        webix.ajax().sync().headers({
            'Content-type': 'application/json'
        }).post(PHSMSAPI.getApiUrl('/z-read/reset'), reset_data, {
            "success": function (text, data, XmlHttpRequest) {
                var response = data.json();
                if (response.status == 'SUCCESS') {
                    app.callEvent("completedResettingZRead");
                } else {
                    webix.message({
                        type: "error",
                        text: "Failed resetting Z Read (FAILURE): " + XmlHttpRequest.statusCode + ": " + XmlHttpRequest.statusText
                    });
                }
            },
            "error": function (text, data, XmlHttpRequest) {
                webix.message({
                    type: "error",
                    text: "Failure resetting Z Read: " + XmlHttpRequest.statusCode + ": " + XmlHttpRequest.statusText
                });
            }
        });
    }

    function _addZReadValue(paymentType, total, data) {
        zReadValues.data.each(function (obj) {
            if (obj.Name == paymentType) {
                obj.Total = total;
                obj.Delta = (obj.Total - obj.TransactedTotal);
                obj.data = data;
            }
        });
    }

    return {
        data: zReadValues,
        addZReadPaymentValue: _addZReadValue,
        submit: _submit,
        reset: _reset,
    }
});