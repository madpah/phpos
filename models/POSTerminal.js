define([
    "app",
    "models/PHSMSAPI",
], function(app, PHSMSAPI) {

    var localConfig = null;
    var configuration = null;

    if (LocalConfig) {
        localConfig = LocalConfig.post;
        webix.storage.local.put('posTerminal', localConfig);
    } else {
        webix.message({
            "type": "error",
            "text": "PHPOSD is not running!"
        })
    }

    function _loadConfiguration() {
        webix.ajax().sync().get(PHSMSAPI.getApiUrl('/pos-terminal/configuration'), {}, {
            success: function (text, data, XmlHttpRequest) {
                configuration = data.json();
            },
            error: function (text, data, XmlHttpRequest) {
                webix.message({
                    type: "error",
                    text: "Error loading POS Terminal configuration. Please contact support. (" + XmlHttpRequest.statusText + ")"
                })
            }
        });
    }

    function _loadConfig() {
        _loadConfiguration();
    }

    function getConfig(forceReload = false) {
        if (!configuration || forceReload) {
            _loadConfig();
        }
        return configuration
    }

    function getUuid() {
        return localConfig.uuid;
    }

    function _getNextTransactionNumber() {
        getConfig(true);
        return configuration.next_transaction_number;
    }

    function _logOutAfterSale() {
        return configuration.logout_after_sale;
    }

    _loadConfig();

    app.on("transactionCompleted", function () {
        if (_logOutAfterSale()) {
            app.callEvent("requestUserLogOut");
        }
    });

    return {
        getConfig: getConfig,
        loadConfig: _loadConfig,
        getUuid: getUuid,
        getNextTransactionNumber: _getNextTransactionNumber,
        logOutAfterSale: _logOutAfterSale,
    };
});