/**
 * Created by pahorton on 15/06/2016.
 */
define([
	"app",
	"models/PHSMSAPI"
], function(app, PHSMSAPI) {

	var collection = new webix.DataCollection();

	function setSearchPhrase(phrase) {
		var result = webix.ajax().sync().post(PHSMSAPI.getApiBaseUrl() + "/inventory/search", "phrase=" + phrase, function(text) {
            collection.clearAll();
			collection.parse(text);
		});
	}

	function byBarcodeScan(barcode) {
        var result = webix.ajax().post(PHSMSAPI.getApiBaseUrl() + "/inventory/search", "phrase=" + barcode, function(text) {
            collection.clearAll();
            collection.parse(text);
            app.callEvent('barcodeScanComplete', collection);
        });
	}

	return {
		collection: collection,
		setSearchPhrase: setSearchPhrase,
		byBarcodeScan: byBarcodeScan
	};
});