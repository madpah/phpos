define([
    "app",
	"models/PHSMSAPI",
], function(app, PHSMSAPI) {

	var current_user = false;

    var sU = false;
    if ((sU = webix.storage.session.get('current_user'))) {
        current_user = sU;
        window.location.href = '/#!/app/main';
    }

    setCurrentUser(current_user);

	function setCurrentUser(value, afterlogin){
		//we need to reload document after login out
		if (!value) {
            webix.storage.session.clear();
			// session.logout().then(function() {
				current_user = null;
				window.location.href = '/#!/login';
			// });

			return;
		}

		//we need to reload document when changing active user
		if (current_user && current_user != value) {
			document.location.reload();
			return;
		}

		current_user = value;

		var isvalid = (current_user.settings && typeof current_user.settings == "string");
		current_user.settings = isvalid ? JSON.parse(current_user.settings) : {};

		webix.extend(current_user.settings, {
			language: "en",
			theme: "siberia:webix",
			notifications: 0
		});

		require(["helpers/locale"], function(locale) { // , "helpers/theme", theme
			//if user has different theme after login - we need to reload page
			if (afterlogin) {
				if (!locale.isNow(current_user.settings.language))
                    //|| !theme.isNow(current_user.settings.theme))
				    	document.location.reload();
			}

			//call save to store values in the local store
			locale.setLang(current_user.settings.language, afterlogin);
			//theme.setTheme(current_user.settings.theme, afterlogin);
		});
	}

	function getCurrentUser() {
		return current_user;
	}

    function isUserAuthenticated() {
        if (current_user && current_user.sR && current_user.sR.length > 10) {
            return true;
        } else {
            return false;
        }
    }

	function saveSetting(key, value, reload) {
		if (current_user) {
			var id = current_user.id,
				settings = current_user.settings;

			if (!settings[key] || value != settings[key]) {
				if (reload)
					document.location.reload();
			}
		}
	}

    function attemptLogin(email, password) {
		webix.storage.session.clear();
		webix.storage.session.put('r', (Math.random()*7));

        webix.ajax().sync().post(PHSMSAPI.getApiUrl('/user/authenticate'), {
			r: webix.storage.session.get('r'),
            email_address: email,
            password: password
        }, {
            success: function(text, data, XmlHttpRequest) {
                var response = data.json();
                if (response.status == 'AUTHENTICATED') {
                    current_user = {
                        sR: response.user.sR,
                        full_name: response.user.fullName,
                        email_address: response.user.email_address
                    }
                    //webix.storage.session.put('key', { key: response.key });
                    webix.storage.session.put('ajax_headers', { Authorization: response.user.sR });
                    webix.storage.session.put('current_user', current_user);

                    setCurrentUser(current_user, true);
                } else {
                    webix.storage.session.clear();
                    console.debug(XmlHttpRequest);

                    webix.message({
                        type: "error",
                        text: XmlHttpRequest.statusText
                    });
                }
            },
            error: function(text, data, XmlHttpRequest) {
                webix.storage.session.clear();

                webix.message({
                    type: "error",
                    text: XmlHttpRequest.statusText
                });
            }
        });

        return isUserAuthenticated();
    }

    function _logOut() {
        setCurrentUser(false);
    }

	return {
		saveSetting: saveSetting,
		getCurrentUser: getCurrentUser,
        logOut: _logOut,
        isUserAuthenticated: isUserAuthenticated,
        attemptLogin: attemptLogin
	};
});