define([
    "app",
    "models/PHSMSAPI",
], function(app, PHSMSAPI) {

    var paymentTypes = new webix.DataCollection({
        id: "dataPaymentTypes",
        data: {},
        url: PHSMSAPI.getApiUrl('/payment-type/all')
    });

    function _getIdForName(name) {
        var i = null;
        paymentTypes.data.each(function (obj) {
            if (obj.name == name) {
                i = obj.id;
                return;
            }
        });

        return i;
    }

    return {
        data: paymentTypes,
        getIdForName: _getIdForName,
    }
});