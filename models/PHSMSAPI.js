define([
], function() {

    var useSSL, apiDomain;

    try {
        useSSL = LocalConfig.api.ssl;
        apiDomain = LocalConfig.api.domain;
    } catch (err) {
        webix.message({
            type: "error",
            text: "PHPOSD not running!"
        });
    }

    function getApiBaseUrl() {
        if (useSSL) {
            return 'https://' + apiDomain;
        } else {
            return 'http://' + apiDomain;
        }
    }

    function getApiUrl(path) {
        return getApiBaseUrl() + path;
    }

    return {
        getApiBaseUrl: getApiBaseUrl,
        getApiUrl: getApiUrl
    };
});