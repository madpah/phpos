define([
    "app",
    "models/PHSMSAPI",
], function(app, PHSMSAPI) {

    var taxClasses = new webix.DataCollection({
        id: "dataTaxClasses",
        data: {},
        url: PHSMSAPI.getApiUrl('/tax-class/get-all-with-rates')
    });

    function _getCodeForClass(classId) {
        var i = taxClasses.getItem(taxClasses.getIdByIndex(classId));
        return i.Code;
    }

    function _getRateForClass(classId) {
        var i = taxClasses.getItem(taxClasses.getIdByIndex(classId));
        return i.Rate;
    }

    function _getRateIdForClass(classId) {
        var i = taxClasses.getItem(taxClasses.getIdByIndex(classId));
        return i.TaxRateId;
    }

    function _getClassesAsArrayOptions() {
        var options = [];
        taxClasses.data.each(function (obj) {
            options.push({
                id: obj.TaxClassId,
                value: obj.Name
            });
        });
        options.shift();
        return options;
    }

    return {
        data: taxClasses,
        getCodeForClass: _getCodeForClass,
        getRateForClass: _getRateForClass,
        getRateIdForClass: _getRateIdForClass,
        getClassesAsOptions: _getClassesAsArrayOptions,
    }
});