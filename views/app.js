define([
	"models/User",
    "views/modules/pos_header"
], function(user, header) {

	var body = {
		rows: [
            {
                $subview: true
			}
		]
	};

	var layout = {
        type: "clean",
		rows: [
            header,
			{
                type: "clean",
				cols: [
					body
				]
			}
		]
	};

	return {
		$ui: layout,
		$oninit: function(view, scope) {
            if (!user.isUserAuthenticated()) {
                return false;
            }
		}
	};
});