define([
    "app",
    "models/POSTerminal",
    "models/User",
], function(app, POSTerminal, User) {

    var posHeader = {
        view: "toolbar",
        elements: [
            { view: "label", label: "PHPOS", width: 200 },
            {},
            {
                view: "label", label: "", id: "posHeaderPOSLocation", css: "pos_header_info_item"
            },
            {
                view: "label", label: "", id: "posHeaderPOSName", css: "pos_header_info_item"
            },
            {
                view: "label", label: "", id: "posHeaderTransactionNumber", css: "pos_header_info_item"
            },
            {
                view: "label", label: "", id: "posHeaderUsername", css: "pos_header_info_item"
            },
            {}
        ]
    };

    return {
        $ui: posHeader,
        $oninit: function (view, $scope) {
            $scope.on(app, "transactionStarted", function(tData) {
                $$("posHeaderTransactionNumber").setValue(tData.number.substring(12));
            });

            $scope.on(app, "userAuthenticated", function (currentUser) {
                console.debug("User Auth in Header");
                $$("posHeaderUsername").setValue(currentUser.full_name);
            });

            $$("posHeaderPOSLocation").setValue(POSTerminal.getConfig().location.name);
            $$("posHeaderPOSName").setValue(POSTerminal.getConfig().name);

            if (User.isUserAuthenticated()) {
                $$("posHeaderUsername").setValue(User.getCurrentUser().full_name);
            } else {
                $$("posHeaderUsername").setValue("Not logged in");
            }
        }
    }
});


