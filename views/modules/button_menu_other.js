define([
    "app",
    "models/Transaction"
], function(app, Transaction) {

    var layout = {
        id: "buttonSetOther",
        hidden: true,
        rows: [
            {
                cols: [
                    { view: "button", height: 120, label: "X Read", click: _buttonXRead },
                    { view: "button", height: 120, label: "Z Read", click: _buttonZRead },
                ]
            },
            {
                cols: [
                    { view: "button", height: 120, label: "", disabled: true },
                    { view: "button", height: 120, label: "", disabled: true },
                ]
            },
            {
                cols: [
                    { view: "button", height: 120, label: "Open Drawer", click: _buttonOpenDrawer },
                    { view: "button", height: 120, label: "Print Transaction", click: _buttonPrintTransaction },
                ]
            },
            {
                cols: [
                    { view: "button", height: 120, label: "", disabled: true },
                    { view: "button", height: 120, label: "", disabled: true },
                ]
            },
            {
                cols: [
                    { view: "button", height: 150, label: "Back [F12]", click: function () {
                        app.callEvent("togglePOSMainButtonSet");
                    } },
                ]
            },
        ]
    }

    function _buttonXRead() {
        app.callEvent("executeXRead");
    }

    function _buttonZRead() {
        app.callEvent("executeZRead");
    }

    function _buttonOpenDrawer() {
        app.callEvent('executeOpenDrawer');
    }

    function _buttonPrintTransaction() {
        if (Transaction.isInProgress()) {
            app.callEvent('printCurrentTransaction');
        }
    };

    return {
        $ui: layout,
        //$oninit: function (view, $scope) {}
    }
});