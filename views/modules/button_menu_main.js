define([
    "app",
    "models/Transaction"
], function(app, Transaction) {

    var layout = {
        id: "buttonSetMain",
        rows: [
            {
                cols: [
                    { id: "mainButtonSetButtonDeleteLine", view: "button", height: 120, label: "Delete Line [F1]", disabled: true,  click: _buttonDeleteLine },
                    { id: "mainButtonSetButtonChangeQuantity", view: "button", height: 120, label: "Quantity [F2]", disabled: true , click: _buttonChangeQuantity },
                ]
            },
            {
                cols: [
                    { id: "mainButtonSetButtonChangePrice", view: "button", height: 120, label: "Discount Line [F3]", disabled: true, click: _buttonChangePrice  },
                    { id: "mainButtonSetButtonRefundLine", view: "button", height: 120, label: "Refund Item [F4]", disabled: true, click: _buttonRefundLine },
                ]
            },
            {
                cols: [
                    { view: "button", height: 120, label: "Customer [F5]", disabled: true },
                    { id: "mainButtonSetButtonMiscSale", view: "button", height: 120, label: "Misc Sale [F6]", click: _buttonMiscSale },
                ]
            },
            {
                cols: [
                    { id: "mainButtonSetButtonPay", view: "button", height: 150, label: "Pay [F10]", disabled: true, click: _buttonPay },
                ]
            },
            {
                cols: [
                    { view: "button", height: 150, label: "Other [F12]", click: _buttonOther },
                ]
            },
            {
                cols: [
                    { id: "mainButtonSetButtonCancelSale", view: "button", height: 120, label: "Cancel Sale [F9]", disabled: true, click: _buttonAttemptCancelSale },
                    { view: "button", height: 120, hotkey: "ALT+ESC", label: "Log out [ALT+ESC]", click: _buttonAttemptLogOut },
                ]
            },
        ]
    }

    function _handleTransactionChange(id, index) {
        if (Transaction.lData.count() > 0) {
            $$("mainButtonSetButtonDeleteLine").enable();
            $$("mainButtonSetButtonChangeQuantity").enable();
            $$("mainButtonSetButtonChangePrice").enable();
            $$("mainButtonSetButtonRefundLine").enable();
            $$("mainButtonSetButtonMiscSale").enable();
            $$("mainButtonSetButtonPay").enable();
            $$("mainButtonSetButtonCancelSale").enable();
        } else {
            $$("mainButtonSetButtonDeleteLine").disable();
            $$("mainButtonSetButtonChangeQuantity").disable();
            $$("mainButtonSetButtonChangePrice").disable();
            $$("mainButtonSetButtonRefundLine").disable();
            //$$("mainButtonSetButtonMiscSale").disable();
            $$("mainButtonSetButtonPay").disable();
            $$("mainButtonSetButtonCancelSale").disable();
        }
    }

    function _buttonDeleteLine() {
        if (Transaction.lData.count() > 0) {
            app.callEvent("deleteTransactionLine");
        }
    }

    function _buttonChangeQuantity() {
        if (Transaction.lData.count() > 0) {
            app.callEvent("changeTransactionLineQuantity");
        }
    }

    function _buttonChangePrice() {
        if (Transaction.lData.count() > 0) {
            app.callEvent("changeTransactionLinePrice");
        }
    }

    function _buttonRefundLine() {
        if (Transaction.lData.count() > 0) {
            app.callEvent("changeTransactionRefundLine");
        }
    }

    function _buttonMiscSale() {
        app.callEvent("addMiscItemTransactionLine");
    }

    function _buttonPay() {
        if (Transaction.lData.count() > 0) {
            app.callEvent("beginSalePayment");
        }
    }

    function _buttonOther() {
        app.callEvent("togglePOSButtonSet");
    }

    function _buttonAttemptCancelSale() {
        if (Transaction.isInProgress()) {
            webix.confirm("Are you sure you want to cancel the current sale?", function (result) {
                if (result) {
                    app.callEvent("cancelCurrentSaleNow");
                    _handleTransactionChange();
                }
            });
        } else {
            app.callEvent("cancelCurrentSaleNow");
            _handleTransactionChange();
        }
    }

    function _buttonAttemptLogOut() {
        if (Transaction.isInProgress()) {
            webix.confirm("Are you sure you want to Log Out? The current sale will be lost!", function (result) {
                if (result) {
                    //User.logOut();
                    app.callEvent("requestUserLogOut");
                }
            });
        } else {
            //User.logOut();
            app.callEvent("requestUserLogOut");
        }
    }

    return {
        $ui: layout,
        $oninit: function (view, $scope) {
            Transaction.lData.attachEvent("onAfterAdd", _handleTransactionChange);
            Transaction.lData.attachEvent("onAfterDelete", _handleTransactionChange);
            $scope.on(app, "transactionStarted", _handleTransactionChange);

            document.addEventListener("keydown", function (e) {
                // Handle Function Key presses
                if (e.keyCode >= 112 && e.keyCode <= 123) {
                    switch (e.keyCode) {
                        case 112:
                            // F1
                            _buttonDeleteLine();
                            break;
                        case 113:
                            // F2
                            _buttonChangeQuantity();
                            break;
                        case 114:
                            // F3
                            _buttonChangePrice();
                            break;
                        case 115:
                            // F4
                            _buttonRefundLine();
                            break;
                        case 116:
                            // F5
                            break;
                        case 117:
                            // F6
                            _buttonMiscSale();
                            break;
                        case 118:
                            // F7
                            break;
                        case 119:
                            // F8
                            break;
                        case 120:
                            // F9
                            _buttonAttemptCancelSale();
                            break;
                        case 121:
                            // F10
                            _buttonPay();
                            break;
                        case 122:
                            // F11
                            // MAC: This is an OS key call
                            // Windows: Expect this if Fullscreen mode toggle
                            break;
                        case 123:
                            // F12
                            _buttonOther();
                            break;
                    }
                }
            }, false);
        }
    }
});