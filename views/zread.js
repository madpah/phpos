define([
    "app",
    "models/User",
    "models/ZRead",
], function(app, User, ZRead) {

    // Make sure we have an authenticated User
    if (!User.isUserAuthenticated()) {
        User.logOut();
        return false;
    }

    webix.rules.zReadValue = function (val, obj, key) {
        var parts = key.split('.');
        if (isNaN(parseFloat(val))) {
            return false;
        }
        var denomination = parseInt(parts[2]);
        var penceValue = Math.floor(parseFloat(val) * 100);
        if ((penceValue % denomination) == 0) {
            return true;
        } else {
            return false;
        }
    }

    var layout = {
        type: "clean",
        rows: [
            {
                template: "End of Day Z Read", height: 50, type: "header"
            },
            {
                height: 20,
            },
            {
                id: "posTerminalZReadAccordion",
                type: "accordion",
                multi: false,
                //minHeight: 500,
                cols: [
                    {
                        id: "posTerminalZReadAccordionStep1",
                        header: "1. Count",
                        body: {
                            id: "posTerminalZReadAccordionStep1Form",
                            view: "form",
                            elementsConfig: {
                                labelWidth: 150,
                            },
                            elements: [
                                {
                                    gravity: 0.5, template: "Count the contents to the till drawer and enter thev values on this page. Enter zero values where required."
                                },
                                {
                                    cols: [
                                        {
                                            rows: [
                                                { template: "Cash", type: "header" },
                                                { id: "zReadCash5000", view: "text", name: "zRead.Cash.5000", label: "£ 50", width: 250 },
                                                { id: "zReadCash2000", view: "text", name: "zRead.Cash.2000", label: "£ 20", width: 250 },
                                                { id: "zReadCash1000", view: "text", name: "zRead.Cash.1000", label: "£ 10", width: 250 },
                                                { id: "zReadCash0500", view: "text", name: "zRead.Cash.0500", label: "£  5", width: 250 },
                                                {},
                                                { id: "zReadCash0200", view: "text", name: "zRead.Cash.0200", label: "£  2", width: 250 },
                                                { id: "zReadCash0100", view: "text", name: "zRead.Cash.0100", label: "£  1", width: 250 },
                                                {},
                                                { id: "zReadCash0050", view: "text", name: "zRead.Cash.0050", label: " 50p", width: 250 },
                                                { id: "zReadCash0020", view: "text", name: "zRead.Cash.0020", label: " 20p", width: 250 },
                                                { id: "zReadCash0010", view: "text", name: "zRead.Cash.0010", label: " 10p", width: 250 },
                                                { id: "zReadCash0005", view: "text", name: "zRead.Cash.0005", label: "  5p", width: 250 },
                                                { id: "zReadCash0002", view: "text", name: "zRead.Cash.0002", label: "  2p", width: 250 },
                                                { id: "zReadCash0001", view: "text", name: "zRead.Cash.0001", label: "  1p", width: 250 },
                                            ]
                                        },
                                        {},
                                        {
                                            rows: [
                                                { template: "Card", type: "header" },
                                                { id: "zReadCardAmex", view: "text", name: "zRead.Card.Amex", label: "Amex", width: 250 },
                                                { id: "zReadCardMasterCard", view: "text", name: "zRead.Card.MasterCard", label: "MasterCard", width: 250 },
                                                { id: "zReadCardVisa", view: "text", name: "zRead.Card.Visa", label: "Visa", width: 250 },
                                                { id: "zReadCardOther", view: "text", name: "zRead.Card.Other", label: "Other Cards", width: 250 },
                                            ]
                                        },
                                        {},
                                        {
                                            rows: [
                                                { template: "Other", type: "header" },
                                                { id: "zReadCheque", view: "text", name: "zRead.Cheque", label: "Cheque", width: 250 },
                                                {},
                                                { id: "zReadAmazon", view: "text", name: "zRead.Amazon", label: "Amazon", width: 250 },
                                                {},
                                                { id: "zReadPayPal", view: "text", name: "zRead.PayPal", label: "PayPal", width: 250 },
                                            ]
                                        }
                                    ]
                                },
                                {},
                                {
                                    cols: [
                                        {},
                                        {},
                                        {},
                                        {
                                            id: "posTerminalZReadAccordionStep1Next",
                                            view: "button",
                                            label: "Next",
                                            type: "next",
                                            hotkey: "enter",
                                            click: function () {
                                                if ($$("posTerminalZReadAccordionStep1Form").validate()) {
                                                    var zReadTotals = new Object;
                                                    var zReadTotalsData = new Object;
                                                    var values = $$("posTerminalZReadAccordionStep1Form").getValues();
                                                    for (var key in values) {
                                                        var keyParts = key.split('.');
                                                        var value = parseFloat(values[key]);

                                                        if (!zReadTotals[keyParts[1]]) {
                                                            zReadTotals[keyParts[1]] = 0.00;
                                                        }
                                                        if (!zReadTotalsData[keyParts[1]]) {
                                                            zReadTotalsData[keyParts[1]] = new Object;
                                                        }

                                                        zReadTotals[keyParts[1]] += value;
                                                        if (keyParts.length == 3) {
                                                            zReadTotalsData[keyParts[1]][keyParts[2]] = value;
                                                        } else {
                                                            zReadTotalsData[keyParts[1]] = value;
                                                        }
                                                    }

                                                    for (var paymentType in zReadTotals) {
                                                        ZRead.addZReadPaymentValue(paymentType, zReadTotals[paymentType], zReadTotalsData[paymentType]);
                                                    }

                                                    console.debug("Begin sync");
                                                    $$("posTerminalZReadAccordionStep2DataTable").sync(ZRead.data);
                                                    console.debug("Go to step 2");
                                                    $$("posTerminalZReadAccordionStep2").expand();
                                                } else {
                                                    $$("zReadCash5000").focus();
                                                }
                                            }
                                        }
                                    ]
                                }
                            ],
                            rules: {
                                "zRead.Cash.5000": webix.rules.zReadValue,
                                "zRead.Cash.2000": webix.rules.zReadValue,
                                "zRead.Cash.1000": webix.rules.zReadValue,
                                "zRead.Cash.0500": webix.rules.zReadValue,
                                "zRead.Cash.0200": webix.rules.zReadValue,
                                "zRead.Cash.0100": webix.rules.zReadValue,
                                "zRead.Cash.0050": webix.rules.zReadValue,
                                "zRead.Cash.0020": webix.rules.zReadValue,
                                "zRead.Cash.0010": webix.rules.zReadValue,
                                "zRead.Cash.0005": webix.rules.zReadValue,
                                "zRead.Cash.0002": webix.rules.zReadValue,
                                "zRead.Cash.0001": webix.rules.zReadValue,
                                "zRead.Card.Amex": webix.rules.isNumber,
                                "zRead.Card.MasterCard": webix.rules.isNumber,
                                "zRead.Card.Visa": webix.rules.isNumber,
                                "zRead.Card.Other": webix.rules.isNumber,
                                "zRead.Cheque": webix.rules.isNumber,
                                "zRead.Amazon": webix.rules.isNumber,
                                "zRead.PayPal": webix.rules.isNumber,
                            }
                        }
                    },
                    {
                        id: "posTerminalZReadAccordionStep2",
                        header: "2. Verify",
                        collapsed: true,
                        body: {
                            rows: [
                                {
                                    id: "posTerminalZReadAccordionStep2DataTable",
                                    view: "datatable",
                                    autoconfig: true,
                                    // math: true,
                                    columns: [
                                        { id: "id", hidden: true },
                                        { id: "Name", header: "Payment Type", fillspace: true },
                                        {
                                            id: "Total",
                                            header: "Counted",
                                            width: 150,
                                            format: webix.i18n.priceFormat,
                                            footer: { content: "summColumn" }
                                        },
                                        {
                                            id: "TransactedTotal",
                                            header: "Transacted Total",
                                            width: 150,
                                            format: webix.i18n.priceFormat,
                                            footer: { content: "summColumn" }
                                        },
                                        {
                                            id: "Delta",
                                            header: "Difference",
                                            width: 150,
                                            format: webix.i18n.priceFormat,
                                            cssFormat: function (val) {
                                                if (parseFloat(val.substring(1)) < 0.00) {
                                                    return { "color": "RED", "font-weight": "bold" };
                                                }
                                            },
                                            footer: { content: "summColumn" }
                                        },
                                    ],
                                    footer: true,
                                },
                                {},
                                {
                                    cols: [
                                        {},
                                        {},
                                        {},
                                        {
                                            id: "posTerminalZReadAccordionStep2Next",
                                            view: "button",
                                            label: "Submit Z Read & Reset",
                                            type: "next",
                                            click: function () {
                                                webix.confirm({
                                                    title: "Are you sure?",
                                                    text: "Are you sure you are ready to reset? This cannot be undone!",
                                                    ok: "RESET NOW",
                                                    type: "confirm-warning",
                                                    callback: function (result) {
                                                        if (result) {
                                                            app.callEvent("beginSubmitZReadAndSubmit");
                                                        }
                                                    }
                                                });

                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    },
                    {
                        id: "posTerminalZReadAccordionStep3",
                        header: "3. Complete",
                        collapsed: true,
                        body: {
                            rows: [
                                {},
                                {
                                    cols: [
                                        {},
                                        { id: "posTerminalZReadAccordionStep3StatusMessage", view: "text", height: 80, readonly: true },
                                        {},
                                        { id: "posTerminalZReadAccordionStep3AcknowledgeButton", view: "button",  value: "Acknowledge", disabled: true, click: function () {
                                            User.logOut();
                                        } },
                                        {}
                                    ]
                                },
                                {}
                            ]
                        }
                    }
                ]
            }
        ]
    }

    return {
        $ui: layout,
        $oninit: function ($view, $scope) {
            webix.extend($$("posTerminalZReadAccordionStep3StatusMessage"), webix.ProgressBar);

            $$("zReadCash5000").focus();

            $scope.on(app, "beginSubmitZReadAndSubmit", function() {
                $$("posTerminalZReadAccordionStep3").expand();
                $$("posTerminalZReadAccordionStep3StatusMessage").showProgress({
                    position: 0.00,
                    type: "bottom"
                });
                $$("posTerminalZReadAccordionStep3StatusMessage").setValue("Submitting Z Read...");
                ZRead.submit();
            });

            $scope.on(app, "completedSubmittingZRead", function (reset_data) {
                $$("posTerminalZReadAccordionStep3StatusMessage").showProgress({
                    position: 0.50,
                    type: "bottom"
                });
                $$("posTerminalZReadAccordionStep3StatusMessage").setValue("Resetting...");
                ZRead.reset(reset_data);
            });

            $scope.on(app, "completedResettingZRead", function () {
                $$("posTerminalZReadAccordionStep3StatusMessage").showProgress({
                    position: 1.00,
                    type: "bottom"
                });
                $$("posTerminalZReadAccordionStep3StatusMessage").setValue("Complete!");
                $$("posTerminalZReadAccordionStep3AcknowledgeButton").enable();
            });
        }
    }
});