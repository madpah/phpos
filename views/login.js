define([
    "models/POSTerminal",
    "models/User.js",
], function(POSTerminal, user) {

	var layout = {
        type: "clean",
        rows: [
            {
                view: "toolbar",
                elements: [
                    { view: "label", label: "PHPOS", width: 200 },
                    {},
                    {
                        view: "label", label: "", id: "posHeaderPOSLocation", css: "pos_header_info_item"
                    },
                    {
                        view: "label", label: "", id: "posHeaderPOSName", css: "pos_header_info_item"
                    },
                    {},
                    {},
                    {}
                ]
            },
            {
                rows: [
                    {},
                    {
                        cols: [
                            {},
                            {
                                id: "userLoginForm",
                                view: "form",
                                elementsConfig: {
                                    labelWidth: 200
                                },
                                elements: [
                                    { view: "text", name: "email_address", label: "E-mail Address", id: "userLoginEmailAddress" },
                                    { view: "text", name: "password", label: "Password", id: "userLoginPassword", type: "password" },
                                    {
                                        cols: [
                                            {},
                                            {},
                                            { view: "button", hotkey: "enter", value: "Login", click: function() {
                                                if ($$("userLoginForm").validate()) {
                                                    var result = user.attemptLogin($$("userLoginEmailAddress").getValue(), $$("userLoginPassword").getValue());
                                                    console.debug("Login result: " + result);
                                                    if (result) {
                                                        // Load main app/dashboard
                                                        window.location.href = '/#!/app/main';
                                                    } else {
                                                        // Reset login form
                                                        $$("userLoginEmailAddress").setValue("");
                                                        $$("userLoginPassword").setValue("")
                                                        $$("userLoginEmailAddress").focus();
                                                    }
                                                } else {
                                                    // $$("userLoginEmailAddress").setValue("");
                                                    // $$("userLoginPassword").setValue("")
                                                    $$("userLoginEmailAddress").focus();
                                                }
                                            }}
                                        ]
                                    }
                                ],
                                rules: {
                                    "email_address": webix.rules.isEmail,
                                    "password": webix.rules.isNotEmpty,
                                },
                            },
                            {}
                        ]
                    },
                    {}
                ]
            },
        ]
    }

	return {
		$ui: layout,
		$oninit: function(view, $scope) {
            $$("posHeaderPOSLocation").setValue(POSTerminal.getConfig().location.name);
            $$("posHeaderPOSName").setValue(POSTerminal.getConfig().name);
            $$("userLoginEmailAddress").focus();
        }
	};
});