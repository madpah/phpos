define([
    "app",
    "models/Transaction",
    "models/Inventory",
    "models/User",
    "models/TaxClass",
    "views/modules/button_menu_main",
    "views/modules/button_menu_other",
], function(app, Transaction, Inventory, User, TaxClass, button_menu_main, button_menu_other) {

    // Make sure we have an authenticated User
    if (!User.isUserAuthenticated()) {
        User.logOut();
        return false;
    }

	var layout = {
		type: "clean",
		rows: [
			{
				type: "space",
				cols: [
					{
                        // Left Main Screen
						type: "clean",
                        view: "form",
                        id: "transactionMainForm",
                        rows: [
                            { view: "text", id: "transactionMainFormBarcodeScanEntry", placeholder: "Scan or enter barcode", labelPosition:"top", height: 75, on: {
                                onKeyPress: function (code, event) {
                                    if (13 == code && this.getValue().length > 2) {
                                        console.debug("Barcode Scan: " + this.getValue());
                                        var barcode = this.getValue();
                                        Inventory.byBarcodeScan(barcode);
                                    }
                                }
                            } },
                            {
                                id: "transactionLineDataTable",
                                view: "datatable",
                                minHeight: 500,
                                navigation: true,
                                columns: [
                                    { id: "id", header: "#", width: 50 },
                                    { id: "name", header: "Item", width: 500, fillspace: true },
                                    { id: "inventory_unit_price", name: "inventory_unit_price", header: "Price", width: 100, format: webix.i18n.priceFormat },
                                    { id: "quantity", name: "quantity", header: "Quantity", width: 100 },
                                    { id: "line_total", name: "total", header: "Total", width: 100, format: webix.i18n.priceFormat },
                                    { id: "tax_class_code", name: "tax_class_code", header: "T", width: 50 }
                                ],
                                data: [],
                                select: "row",
                                scrollY: true,
                                scrollAlignY: true,
                                on: {
                                    onSelectChange: function () {
                                        this.showItem(this.getSelectedId(true).shift());
                                    }
                                }
                            },
                            {
                                height: 20,
                            },
                            {
                                cols: [
                                    {
                                        minHeight: 150,
                                        gravity: 1.2,
                                        id: "transactionTaxBreakdownDataTable",
                                        view: "datatable",
                                        columns: [
                                            { id: "tax_class_code", header: "Code", width: 150 },
                                            { id: "line_total_tax", header: "Total", width: 100, format: function(value) {
                                                return webix.i18n.priceFormat(value);
                                            }},
                                        ],
                                        data: [],
                                        scroll: false
                                    },
                                    {},
                                    {
                                        id: "transactionRunningTotal",
                                        align: "right",
                                        borderless: true,
                                        css: "transaction_total",
                                        view: "text",
                                        value: "0.00",
                                        label: "£",
                                        readonly: true,
                                        disabled: true,
                                    }
                                ]
                            }
                        ]
					},
                    {
                        // Right Button Menu
                        id: "posMainScreenButtonMenuContainer",
                        gravity: 0.4,
                        type: "clean",
                        cols: [
                            button_menu_main,
                            button_menu_other
                        ]
                    }
				]
			},
		]
	};

    webix.GroupMethods.taxSummary = function(prop, data) {
        if (!data.length) return 0;
        var summ = 0;
        for (var i = data.length - 1; i >= 0; i--) {
            summ += prop(data[i])*1;
        };
        return summ;
    };

    function _handleTransactionChange() {
        $$("transactionRunningTotal").setValue(Transaction.getTotal());
        if (Transaction.lData.count() > 0) {
            $$("transactionLineDataTable").select(Transaction.lData.getLastId());
        }
        $$("transactionMainFormBarcodeScanEntry").setValue("");
        $$("transactionMainFormBarcodeScanEntry").focus();
    }

    function _handleBeforeTransactionLineChange() {
        $$("transactionTaxBreakdownDataTable").ungroup();
    }

    function _handleAfterTransactionLineChange() {
        $$("transactionTaxBreakdownDataTable").group({
            by: "tax_class_id",
            map: {
                tax_class_code: [ "tax_class_code" ],
                line_total_tax: [ "line_total_tax", "sum" ]
            }
        }, true);
        $$("transactionLineDataTable").refresh();
        _handleTransactionChange();
    }

	return {
		$ui: layout,
        $oninit: function (view, $scope) {
            Transaction.start();
            $$("transactionMainFormBarcodeScanEntry").focus();
            $$("transactionLineDataTable").sync(Transaction.lData);
            $$("transactionTaxBreakdownDataTable").sync(Transaction.lData);

            $scope.on(app, "transactionStarted", _handleTransactionChange);
            $scope.on(app, "transactionLineUpdated", _handleAfterTransactionLineChange);
            $scope.on(app, "backToMainPOS", _handleAfterTransactionLineChange);
            //$scope.on(app, "transactionCompleted", _handleTransactionChange);

            $scope.on(app, "requestUserLogOut", function () {
                User.logOut();
            });

            $scope.on(app, "togglePOSButtonSet", function () {
                // buttonSetMain buttonSetOther
                console.debug("togglePOSButtonSet");
                if ($$("buttonSetMain").isVisible()) {
                    $$("buttonSetMain").hide();
                    $$("buttonSetOther").show();
                } else if ($$("buttonSetOther").isVisible()) {
                    $$("buttonSetMain").show();
                    $$("buttonSetOther").hide();
                } else {
                    $$("buttonSetMain").show();
                    $$("buttonSetOther").hide();
                }
            });

            Transaction.lData.attachEvent("onBeforeAdd", _handleBeforeTransactionLineChange);
            Transaction.lData.attachEvent("onBeforeDelete", _handleBeforeTransactionLineChange);

            Transaction.lData.attachEvent("onAfterAdd", _handleAfterTransactionLineChange);
            Transaction.lData.attachEvent("onAfterDelete", _handleAfterTransactionLineChange);

            $scope.on(app, "barcodeScanComplete", function () {
                if (Inventory.collection.count() == 1) {
                    var i = Inventory.collection.getItem(Inventory.collection.getFirstId());
                    Transaction.addLine({
                        inventory_id: i.id,
                        inventory_name: (i.variantName.length > 0 ? i.name + ' - ' + i.variantName : i.name),
                        inventory_barcode: (i.inventoryBarcode ? i.inventoryBarcode.barcodeValue : ""),
                        inventory_unit_price: i.unitPriceIncTax,
                        inventory_unit_tax: i.unitTax,
                        tax_class_id: i.taxClassId,
                        quantity: (i.inventoryBarcode ? i.inventoryBarcode.inventoryQuantity : 1)
                    });
                } else if (Inventory.collection.count() > 1) {
                    webix.ui({
                        view: "window",
                        id: "inventorySearchWindow",
                        head: {
                            view: "toolbar",
                            cols: [
                                { view: "label", label: "Inventory Search Results" },
                                {},
                                {},
                                { view: "icon", icon: "close", hotkey: "esc", click: "$$('inventorySearchWindow').close();" }
                            ]
                        },
                        modal: true,
                        fullscreen: true,
                        position: "center",
                        body: {
                            type: "space",
                            rows: [
                                {
                                    id: "inventorySearchWindowDataTable",
                                    view: "datatable",
                                    autoconfig: true,
                                    gravity: 2.2,
                                    navigation: true,
                                    select: "row",
                                    columns: [
                                        { id: "id", hidden: true },
                                        { id: "sku", header: "SKU", width: 175 },
                                        { id: "name", header: "Name", fillspace: true},
                                        { id: "variantName", header: "Variant", width: 200 },
                                        { id: "unitPriceIncTax", header: "Price", format: webix.i18n.priceFormat, width: 120 },
                                        { id: "stockLevel", header: "Current Stock", width: 120 },
                                    ],
                                    on: {
                                        "onKeyPress": function (code, e) {
                                            if (code == 13) {
                                                var i = $$("inventorySearchWindowDataTable").getSelectedItem();
                                                Transaction.addLine({
                                                    inventory_id: i.id,
                                                    inventory_name: (i.variantName.length > 0 ? i.name + ' - ' + i.variantName : i.name),
                                                    inventory_barcode: i.inventoryBarcode.barcodeValue,
                                                    inventory_unit_price: i.unitPriceIncTax,
                                                    inventory_unit_tax: i.unitTax,
                                                    tax_class_id: i.taxClassId,
                                                    quantity: i.inventoryBarcode.inventoryQuantity
                                                });
                                                $$("inventorySearchWindow").close();
                                            }
                                        }
                                    }
                                },
                            ]
                        },
                        on: {
                            "onShow": function () {
                                $$('inventorySearchWindowDataTable').parse(Inventory.collection);
                                $$("inventorySearchWindowDataTable").select(Inventory.collection.getIdByIndex(0));
                                webix.UIManager.setFocus($$("inventorySearchWindowDataTable"));
                            },
                            "onDestruct": function () {
                                app.callEvent("backToMainPOS");
                            }
                        }
                    }).show();
                } else {
                    webix.message({
                        type: "error",
                        text: "Product not found"
                    })
                }
            });

            $scope.on(app, "deleteTransactionLine", function () {
                var selected = $$("transactionLineDataTable").getSelectedId();
                if (selected) {
                    Transaction.removeLine(selected);
                }
            });

            $scope.on(app, "changeTransactionLineQuantity", function () {
                var selected = $$("transactionLineDataTable").getSelectedId();
                if (selected) {
                    webix.ui({
                        view: "window",
                        id: "transactionMainModalChangeLineQuantity",
                        head: "Change Line Quantity",
                        modal: true,
                        width: 600,
                        height: 400,
                        position: "center",
                        body: {
                            type: "space",
                            rows: [
                                {
                                    id: "transactionMainModalChangeLineQuantityInput",
                                    view: "text",
                                    label: "Current Quantity: " + Transaction.lData.getItem(selected).quantity,
                                    labelPosition: "top",
                                    validate: webix.rules.isNumber
                                },
                                {
                                    cols: [
                                        {
                                            view: "button", label: "Cancel", hotkey: "esc", click: function () {
                                                $$("transactionMainModalChangeLineQuantity").close();
                                            }
                                        },
                                        {},
                                        {
                                            view: "button", label: "Apply", hotkey: "enter", type: "danger", click: function () {
                                                var val = parseInt($$("transactionMainModalChangeLineQuantityInput").getValue());
                                                if (val && val > 0) {
                                                    Transaction.changeLineQuantity($$("transactionLineDataTable").getSelectedId(), val);
                                                    $$("transactionMainModalChangeLineQuantity").close();
                                                } else {
                                                    $$("transactionMainModalChangeLineQuantityInput").focus();
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        on: {
                            "onShow": function () {
                                $$("transactionMainModalChangeLineQuantityInput").focus();
                            },
                            "onDestruct": function () {
                                app.callEvent("backToMainPOS");
                            }
                        }
                    }).show();
                } else {
                    webix.message({
                        type: "error",
                        text: "Please select a line first!"
                    })
                }
            });

            $scope.on(app, "changeTransactionLinePrice", function () {
                var selected = $$("transactionLineDataTable").getSelectedId();
                if (selected) {
                    webix.ui({
                        view: "window",
                        id: "transactionMainModalChangeLinePrice",
                        head: "Change Line Price",
                        modal: true,
                        width: 600,
                        height: 400,
                        position: "center",
                        body: {
                            type: "space",
                            rows: [
                                {
                                    id: "transactionMainModalChangeLinePriceInput",
                                    view: "text",
                                    label: "Current Item Price: " + Transaction.lData.getItem(selected).inventory_unit_price,
                                    labelPosition: "top",
                                    validate: webix.rules.isNumber
                                },
                                {
                                    cols: [
                                        {
                                            view: "button", label: "Cancel", hotkey: "esc", click: function () {
                                                $$("transactionMainModalChangeLinePrice").close();
                                            }
                                        },
                                        {},
                                        {
                                            view: "button", label: "Apply", hotkey: "enter", type: "danger", click: function () {
                                                var val = parseFloat($$("transactionMainModalChangeLinePriceInput").getValue());
                                                if (val && val > 0) {
                                                    Transaction.changeLinePrice($$("transactionLineDataTable").getSelectedId(), val);
                                                    $$("transactionMainModalChangeLinePrice").close();
                                                } else {
                                                    $$("transactionMainModalChangeLinePriceInput").focus();
                                                }
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        on: {
                            "onShow": function () {
                                $$("transactionMainModalChangeLinePriceInput").focus();
                            },
                            "onDestruct": function () {
                                app.callEvent("backToMainPOS");
                            }
                        }
                    }).show();
                } else {
                    webix.message({
                        type: "error",
                        text: "Please select a line first!"
                    })
                }
            });

            $scope.on(app, "changeTransactionRefundLine", function () {
                var selected = $$("transactionLineDataTable").getSelectedId();
                if (selected) {
                    webix.ui({
                        view: "window",
                        id: "transactionMainRefundLineWindow",
                        head: "Are you sure you wish to refund the current Transaction Line?",
                        modal: true,
                        width: 700,
                        height: 400,
                        position: "center",
                        body: {
                            type: "space",
                            rows: [
                                {
                                    cols: [
                                        {
                                            view: "button", label: "No", hotkey: "esc", click: function () {
                                                $$("transactionMainRefundLineWindow").close();
                                            }
                                        },
                                        {},
                                        {
                                            view: "button", label: "Yes", hotkey: "enter", type: "danger", click: function () {
                                                var selected = $$("transactionLineDataTable").getSelectedId();
                                                Transaction.refundLine(selected);
                                                $$("transactionMainRefundLineWindow").close();
                                            }
                                        }
                                    ]
                                },
                            ]
                        },
                        on: {
                            "onDestruct": function () {
                                app.callEvent("backToMainPOS");
                            }
                        }
                    }).show();
                } else {
                    webix.message({
                        type: "error",
                        text: "Select a line to refund first!"
                    });
                }
            });

            $scope.on(app, "addMiscItemTransactionLine", function () {
                webix.ui({
                    view: "window",
                    id: "transactionMainModalAddMiscItem",
                    head: "Misc Item",
                    modal: true,
                    width: 600,
                    height: 450,
                    position: "center",
                    body: {
                        type: "space",
                        id: "transactionMainModalAddMiscItemForm",
                        view: "form",
                        elements: [
                            {
                                id: "transactionMainModalAddMiscItemPrice",
                                name: "transactionMainModalAddMiscItemPrice",
                                view: "text",
                                label: "Item Price: ",
                                labelPosition: "top",
                            },
                            {
                                id: "transactionMainModalAddMiscItemDescription",
                                name: "transactionMainModalAddMiscItemDescription",
                                view: "text",
                                label: "Description: ",
                                labelPosition: "top",
                            },
                            {
                                id: "transactionMainModalAddMiscItemTaxClass",
                                name: "transactionMainModalAddMiscItemTaxClass",
                                view: "radio",
                                label: "Tax Class: ",
                                labelPosition: "top",
                                options: TaxClass.getClassesAsOptions()
                            },
                            {
                                cols: [
                                    {
                                        view: "button", label: "Cancel", hotkey: "esc", click: function () {
                                            $$("transactionMainModalAddMiscItem").close();
                                        }
                                    },
                                    {},
                                    {
                                        view: "button", label: "Apply", hotkey: "enter", type: "danger", click: function () {
                                            if ($$("transactionMainModalAddMiscItemForm").validate()) {
                                                Transaction.addMiscSaleLine(
                                                    $$("transactionMainModalAddMiscItemPrice").getValue(),
                                                    $$("transactionMainModalAddMiscItemDescription").getValue(),
                                                    $$("transactionMainModalAddMiscItemTaxClass").getValue()
                                                );
                                                //$$("transactionMainModalAddMiscItem").close();
                                            } else {

                                            }
                                        }
                                    }
                                ]
                            }
                        ],
                        rules: {
                            "transactionMainModalAddMiscItemPrice": webix.rules.isNumber,
                            "transactionMainModalAddMiscItemDescription": webix.rules.isNotEmpty,
                            "transactionMainModalAddMiscItemTaxClass": webix.rules.isNotEmpty
                        }
                    },
                    on: {
                        "onShow": function () {
                            $$("transactionMainModalAddMiscItemPrice").focus();
                        },
                        "onDestruct": function () {
                            app.callEvent("backToMainPOS");
                        }
                    }
                }).show();
            });

            $scope.on(app, "beginSalePayment", function () {
                webix.ui({
                    view: "window",
                    id: "transactionMainModalPayment",
                    head: "Total due: £" + Transaction.getTotal(),
                    modal: true,
                    fullscreen: true,
                    width: 800,
                    height: 600,
                    position: "center",
                    body: {
                        type: "space",
                        rows: [
                            {
                                cols: [
                                    { view: "button", height: 150, label: "Cash [ALT+1]", hotkey: "ALT+1", click: function () {
                                        app.callEvent("tenderCashPayment");
                                    } },
                                    {},
                                    { view: "button", height: 150, label: "Card [ALT+3]", hotkey: "ALT+3", click: function () {
                                        app.callEvent("attemptPayment", [ 'Card', "FULL" ]);
                                    } },
                                    {},
                                    { view: "button", height: 150, label: "Cheque [ALT+5]", hotkey: "ALT+5", click: function () {
                                        app.callEvent("attemptPayment", [ 'Cheque', "FULL" ]);
                                    } },
                                ]
                            },
                            {
                                cols: [
                                    {},
                                    { view: "button", height: 150, label: "PayPal [ALT+7]", hotkey: "ALT+7", click: function () {
                                        app.callEvent("attemptPayment", [ 'PayPal', "FULL" ]);
                                    } },
                                    {},
                                    { view: "button", height: 150, label: "Amazon [ALT+9]", hotkey: "ALT+9", click: function () {
                                        app.callEvent("attemptPayment", [ 'Amazon', "FULL" ]);
                                    } },
                                    {},
                                ]
                            },
                            {},
                            {
                                id: "transactionPaymentsDataTable",
                                view: "datatable",
                                columns: [
                                    { id: "id", header: "ID", hidden: true },
                                    { id: "payment_type_name", header: "Type" },
                                    { id: "amount", header: "Amount", format: function(value) {
                                        return webix.i18n.priceFormat(value);
                                    }},
                                ],
                                data: [],
                                scroll: false
                            },
                            {},
                            {
                                cols: [
                                    {
                                        view: "button", label: "Back", hotkey: "esc", click: function () {
                                            $$("transactionMainModalPayment").close();
                                        }
                                    },
                                    {},
                                    {}
                                ]
                            }
                        ]
                    },
                    on: {
                        "onShow": function () {
                            $$("transactionPaymentsDataTable").sync(Transaction.pData);
                        }
                    }
                }).show();
            });

            $scope.on(app, "tenderCashPayment", function () {
                webix.ui({
                    view: "window",
                    id: "transactionMainModalPaymentCashInput",
                    head: "Cash",
                    modal: true,
                    width: 600,
                    height: 400,
                    position: "center",
                    body: {
                        type: "space",
                        rows: [
                            {
                                id: "transactionMainModalPaymentCashInputTenderedAmount", view: "text", placeholder: "Enter amount tendered"
                            },
                            {
                                id: "transactionMainModalPaymentCashInputChangeAmount", view: "text", label: "Change", labelPosition: "top", value: "", disabled: true, readonly: true
                            },
                            {
                                cols: [
                                    {
                                        view: "button", label: "Cancel", click: function () {
                                            $$("transactionMainModalPaymentCashInput").close();
                                        }
                                    },
                                    {},
                                    {
                                        view: "button", label: "Continue", hotkey: "enter", type: "danger", click: function () {
                                            var tendered = parseFloat($$("transactionMainModalPaymentCashInputTenderedAmount").getValue());
                                            var changeVal = $$("transactionMainModalPaymentCashInputChangeAmount").getValue();
                                            var changeAmount = parseFloat(Math.round((changeVal) * 100) / 100);
                                            var outstandingBalance = Transaction.getTotalOutstanding();

                                            if (changeVal == "NO CHANGE" || (changeVal.length > 0 && changeAmount >= 0.00)) {
                                                // Second enter
                                                var paymentAmount = tendered;
                                                if (changeAmount > 0.00) {
                                                    paymentAmount -= changeAmount;
                                                }
                                                app.callEvent("attemptPayment", [ 'Cash', paymentAmount ]);
                                            } else {
                                                // First enter
                                                if (tendered && tendered > 0.00) {
                                                    var change = Math.round((tendered - outstandingBalance) * 100) / 100;
                                                    if (outstandingBalance <= tendered) {
                                                        $$("transactionMainModalPaymentCashInputChangeAmount").setValue(change);
                                                        $$("transactionMainModalPaymentCashInputTenderedAmount").focus();
                                                    } else {
                                                        $$("transactionMainModalPaymentCashInputChangeAmount").setValue("NO CHANGE");
                                                        $$("transactionMainModalPaymentCashInputTenderedAmount").focus();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                ]
                            }
                        ]
                    },
                    on: {
                        "onShow": function () {
                            console.debug("Shown");
                            $$("transactionMainModalPaymentCashInputTenderedAmount").focus();
                        }
                    }
                }).show();
            });

            $scope.on(app, "attemptPayment", function (paymentType, amount) {
                if (amount == "FULL") {
                    amount = Transaction.getTotalOutstanding();
                }
                if ($$("transactionMainModalPaymentCashInput")) {
                    $$("transactionMainModalPaymentCashInput").close();
                }
                Transaction.addPayment(paymentType, amount);
            });

            $scope.on(app, "transactionFullyPaid", function () {
                $$("transactionMainModalPayment").close();

                webix.ui({
                    view: "window",
                    id: "transactionFinalisationWindow",
                    head: "Completing Transaction",
                    modal: true,
                    width: 600,
                    height: 400,
                    position: "center",
                    body: {
                        type: "space",
                        rows: [
                            {
                                id: "transactionFinalisationWindowStatusMessage", template: "Please wait..."
                            },
                        ]
                    },
                    on: {
                        "onShow": function () {
                            app.callEvent("transactionBeginFinalisation");
                        },
                        "onDestruct": function () {
                            Transaction.nextTransaction();
                        }
                    }
                }).show();
            });

            $scope.on(app, "transactionCompleted", function () {
                $$("transactionFinalisationWindow").close();
            });

            $scope.on(app, "executeXRead", function () {
                webix.ui({
                    view: "window",
                    id: "xReadWindow",
                    head: {
                        view: "toolbar",
                        cols: [
                            { view: "label", label: "X Read" },
                            {},
                            {},
                            { view: "icon", icon: "close", hotkey: "esc", click: "$$('xReadWindow').close();" }
                        ]
                    },
                    modal: true,
                    width: 700,
                    height: 350,
                    position: "center",
                    body: {
                        type: "space",
                        rows: [
                            {
                                // /pos-terminal/x-read
                                id: "xReadWindowPaymentChart",
                                view: "datatable",
                                autoconfig: true,
                                gravity: 1.5,
                                columns: [
                                    { id: "id", hidden: true },
                                    { id: "Name", header: "Payment Type", fillspace: true },
                                    { id: "TransactedTotal", header: "Transacted Total", width: 150, format: webix.i18n.priceFormat, footer: {content:"summColumn"} },
                                ],
                                url: "http://phsms.dev.local/pos-terminal/x-read",
                                footer: true
                            },
                        ]
                    },
                    on: {
                        "onDestruct": function () {
                            app.callEvent("backToMainPOS");
                        }
                    }
                }).show();
            });

            $scope.on(app, "executeZRead", function () {
                window.location.href = '/#!/app/zread';
            });

            $scope.on(app, "executeOpenDrawer", function () {
                var printWindow = window.open("/print.html?template=drawerOpenOnly", "printWindow", "fullscreen=0,height=10,width=10,location=0,menubar=0,resizable=0,scrollbars=0,status=0,titlebar=0,toolbar=0");
                printWindow.print();
                printWindow.close();
            });
        }
	};
});